# Drag and drop attribute directive for Angular 4+

An angular4 module that exports a drag and a drop attribute directives. 


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Support](#support)
- [Demo](#demo)

## Installation
```
$ npm i angular4-drag-drop --save
```


## Usage
To import the module into a module in which you wish to use the directive on components.
```
import { DragDropDirectiveModule} from "angular4-drag-drop";

```
This module exports two attribute directives, 'DragDirective' and 'DropDirective'.  DragDirectve is used with a component from which you want to drag items, while DropDirective is used with the component in which you wish to drop items.  

Both accept inputs for CSS class highlighting. If you do not pass a string then highlighting will be ignored.  Both will emit the dragged item when it is dropped in the 'DroppableDirective' component or element.

'DragDirective' emits an event when dragging is started.

'DropDirective' emits events when a drag enters a drop element, leaves a drop element, and as stated above when an element is dropped.

### DragDirective
```typescript
<div *ngFor="let item of itemsToDrop" [dragDirective]='item' [dragHightlight]="'highlight'" (releaseDrop)="releaseDrop($event)" (startDrag)="startDrag(item)">
</div>
```
'[dragDirective]="item"' Applies the drag directive, and passes it an item.

'[dragHighlight]="'highlight'"' Passes a string to add a css class to the css class list of the element.

'(releaseDrop)="releaseDrop($event)"' Calls a function to act on the dragged item once it is dropped into the drop area.

'(startDrag)="startDrag(item)" Calls a function when the user starts dragging the item. 

### DropDirectve
```typescript
<div dropDirective (dropEvent)="addDropItem($event)" (dragenterEvent)="dragEnter($event)" (dragleaveEvent)="dragLeave()" class="droppable" [dropHighlight]="'highlight'" >
</div>
```
'dropDirective' applies the directive to an element, making it a drop target.
'[dropHighlight]="'highlight'"' passes a string to add a css class to the css class list of the element.  

'(dropEvent)="addDropItem($event)"' calls a function and passes the dropped item when an item is dropped into the drop element.

'(dragenterEvent)="dragEnter($event)"' calls a function and passes the dragged item when an item is dragged into the drop element.

'(dragleaveEvent)="dragLeave($event)"' calls a function and passes the dragged item when the item is dragged out of the drop element.

##demo
https://embed.plnkr.co/UOLION/

