import { Directive, ElementRef, HostListener, Input, Output,
EventEmitter } from '@angular/core';
import { DragDropDirectiveService } from './drag-drop-directive.service';

@Directive({
  selector: '[dropDirective]'
})
export class DropDirective{
  @Input('dropHighlight')
  cssHighlight:string;
	@Output()
	dropEvent:EventEmitter<any> = new EventEmitter();
  @Output()
  dragenterEvent:EventEmitter<any> = new EventEmitter();
  @Output()
  dragleaveEvent:EventEmitter<any> = new EventEmitter();
	private highlighted:boolean = false;
  private dragItem:any;

  constructor(
    private el: ElementRef,
    private dragDropDirectiveService: DragDropDirectiveService
  ) {}

  @HostListener('dragenter') onDragEnter() {
    this.dragItem = this.dragDropDirectiveService.getDragItem();
    this.dragenterEvent.emit(this.dragItem);
  	//this.highlighted = true;
    this.highlight();
  }
  @HostListener('dragleave') onDragLeave(){
    this.dragItem = this.dragDropDirectiveService.getDragItem();
    this.dragleaveEvent.emit(this.dragItem);
  	//this.highlighted = false;
  	this.highlight();
  }
  @HostListener('dragover') onDragOver(){
  	event.preventDefault();
  }
  @HostListener('drop',['$event']) onDrop(event:any){
  	// since html draggable will not transer an object, we need to parse are string
  	let transferredObject = JSON.parse(event.dataTransfer.getData('customObject'));
    let transferredObjectID = event.dataTransfer.getData('customID');
    this.dropEvent.emit(transferredObject);
    this.dragDropDirectiveService.setDropItem(transferredObjectID);
    this.highlight();
  }

  private highlight(){
    if (this.cssHighlight){
      if (!this.highlighted){
        this.el.nativeElement.classList.add(this.cssHighlight);
      } else {
        this.el.nativeElement.classList.remove(this.cssHighlight);
      }
    }
    this.highlighted = !this.highlighted;
  }

}
